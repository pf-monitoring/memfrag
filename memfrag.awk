# Measure memory fragmentation
#
# Based on:
#    "Measuring the Impact of the Linux Memory Manage"
#    by Mel Gorman and Patrick Healy
#    http://citeseerx.ist.psu.edu/viewdoc/download?doi=10.1.1.466.8457&rep=rep1&type=pdf
#
# Usage:
#    awk --file memfrag.awk /proc/buddyinfo
#
# (C) Oleksandr Natalenko <oleksandr@redhat.com>, 2020
#
# Public domain
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE

BEGIN {
	node_column = 2
	zone_column = 4
	first_order_column = 5
}

{
	node = gensub(",", "", "g", $node_column)
	for (i = first_order_column; i <= NF; i++) {
		order = i - first_order_column
		if (maxorder == "") {
			maxorder = NF - first_order_column
		}
		pages[node][$zone_column][order] += 2 ^ order * $i
	}
}

END {
	if (mode == "" || mode == "detailed")
	{
		for (node in pages) {
			printf("Node: %s\n", node)
			for (zone in pages[node]) {
				printf("\tZone: %s\n", zone)
				for (order in pages[node][zone]) {
					totalfree = 0
					for (i = 0; i <= maxorder; i++) {
						totalfree += pages[node][zone][i]
					}
					sum = 0
					for (i = order + 0; i <= maxorder; i++) {
						sum += pages[node][zone][i]
					}
					frag = (totalfree - sum) / totalfree * 100
					printf("\t\tOrder: %s, frag: %f %%\n", order, frag)
				}
			}
		}
	} else if (mode == "average") {
		for (node in pages) {
			for (zone in pages[node]) {
				for (order in pages[node][zone]) {
					current_pages = pages[node][zone][order]
					merged[order] += current_pages
					totalfree += current_pages
				}
			}
		}
		for (order in merged) {
			sum = 0
			for (i = order + 0; i <= maxorder; i++) {
				sum += merged[i]
			}
			frag += (totalfree - sum) / totalfree * 100 / (maxorder + 1)
		}
		printf("%f\n", frag)
	} else if (mode == "opportunity") {
		for (node in pages) {
			for (zone in pages[node]) {
				for (order in pages[node][zone]) {
					current_pages = pages[node][zone][order]
					if (order == maxorder) {
						merged[order] += current_pages
					}
					totalfree += current_pages
				}
			}
		}
		frag = (totalfree - merged[maxorder]) / totalfree * 100
		printf("%f\n", frag)
	}
}
